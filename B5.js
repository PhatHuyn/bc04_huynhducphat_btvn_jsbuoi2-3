function TinhT2CS(){
    var SoInput = document.getElementById("txtSo_B5").value * 1;
    console.log("Bài tập 5 - Tính tổng 2 chữ số");
    console.log('Số nhập vào: ', SoInput);
    var HangChuc, HangDV, result;

    HangChuc = Math.floor(SoInput/10);
    console.log('HangChuc: ', HangChuc);
    HangDV = SoInput % 10;
    console.log('HangDV: ', HangDV);
    
    result = HangChuc + HangDV;
    console.log('Tổng 2 chữ số là: ', result);

    document.getElementById("KQ_5").innerHTML = 
    `<h1>Tổng hai chữ số là: ${result} </h1>`   

    console.log("---------------------------------------");
    
}
